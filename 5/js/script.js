// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.

// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.
// Необов'язкове завдання підвищеної складності
// Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу: https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з id: 1.
// Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.


const root = document.querySelector('.root')

const API = 'https://ajax.test-danit.com/api/json/'
const createUrl = (postFix) => `${API}${postFix}`
const  usersUrl = createUrl('users')
const  postsUrl = createUrl('posts')

const sendRequest = async (url, method = "GET") => {return await fetch(url).then(response => response.json())}
const getUsers = sendRequest(`${usersUrl}`)
const getPosts = sendRequest(`${postsUrl}`)
const deletePost = (postId) => sendRequest(`${API}posts/${postId}`, "DELETE");


class Card {
    constructor(userName, userEmail, title, text, userId, authorId, postId) {
        this.title = title;
        this.text = text;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userId = userId;
        this.authorId = authorId;
        this.postId = postId;
    }
    renderCard = () => {
        const cardItem = document.createElement('div')
        cardItem.className = 'card-item'
        cardItem.id = `card-item-${this.postId}`;
        const button = document.createElement("button");
        button.innerText = "Delete";
        cardItem.append(button);
        cardItem.insertAdjacentHTML('afterbegin', `
            <div class="card-info">
                <p>${this.userName}</p>
                <p><a href="mailto:${this.userEmail}">${this.userEmail}</a></p>
                <p>${this.title}</p>
                <p>${this.text}</p>
            </div>
        `)
        root.append(cardItem)

        button.addEventListener("click", (elem) => {
            const postId = elem.target
            .closest(".card-item")
            .getAttribute("id")
            .slice(10);

            deletePost(postId).then(() => {
                document.querySelector(`#card-item-${postId}`).remove();
            });
        });
    }   
}




getPosts.then((posts)=>{
    console.log(posts);
    posts.map(({userId, title, body, id})=>{
        getUsers.then((user)=>{
            // console.log(user);
            user.map(({id, name, email})=>{
                if (id === userId) {
                    const card = new Card(name, email, title, body, userId, id, id)
                    // console.log(card); 
                    // console.log(id);
                    card.renderCard()
                }
            })
        })
    })
})
