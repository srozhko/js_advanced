// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Когда действие выполняеться на протяжении некоторого времени, тогда действие становиться в очередь с другими функциями и они выполняються поочередно в порядке очереди что и произведет правильный результат. 

// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.
// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Литература:
// Async/await
// async function
// Документація сервісу ip-api.com


const API = 'https://api.ipify.org/?format=json'
const info = 'http://ip-api.com/'

const button = document.querySelector('.button')
const userLoc = document.createElement('div')
userLoc.className = 'user_location';

const sendRequest = async (url) => {
    return await fetch(url)
        .then(response => {
        if (response.ok){
            return response.json()
        } else {
            return new Error('Чтото пошло не так');
        }
    })
}

button.addEventListener('click', async () => { 
    const userIP = await sendRequest(API)
    const getInfo = await sendRequest(`${info}json/${userIP.ip}`)
    const {countryCode, country, regionName, city, zip} = getInfo
    button.after(userLoc)
    userLoc.innerHTML = `
        <p>Континент: ${countryCode}</p>
        <p>Країна: ${country}</p>
        <p>Регіон: ${regionName}</p>
        <p>Місто: ${city}</p>
        <p>Район: ${zip}</p>
    `
})



