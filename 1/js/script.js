// Теоретичне питання
// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Берет от потомка необходимые нам объэкты, методы, функции.

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Это метод который визывает аргументы с класса-потомка. Так как если мы ходим хотим добавить или изменить какие-то новые аргументы в нашем новом классе при этом еще используем аругементы другого класса и не вызовим с помощью данного метода агрументы, то у нас выдаст ошибку.  


// Завдання
// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Література:
// Класи на MDN
// Класи в ECMAScript 6


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set name (value) {
       this._name = value;
    }

    get name () {
        return this._name
    }

    set age (value) {
        this._age = value;
     }
 
    get age () {
        return this._age
    }

    set salary (value) {
        this._salary = value;
     }
 
    get salary () {
        return this._salary
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang
    }

    set salary (value) {
        this._salary = value * 3
    }

    get salary () {
        return this._salary * 3;
    }
}

// const user = new Employee("Stas", "28", "1200", "uk");
// console.log('user', user);
// console.log('name', user._name);
// console.log('age', user._age);
// console.log('salary', user._salary);
// console.log('lang', user.lang);

const frontend = new Programmer("Stas", "28", 1200, "uk");
// const frontend = new Programmer();
// frontend.name = "Stas";
// frontend.age = "28";
frontend.salary = 1200;
// frontend.lang = "uk";
console.log('frontend', frontend);
// console.log('name', frontend._name);
// console.log('age', frontend._age);
// console.log('salary', frontend._salary);
// console.log('lang', frontend.lang);


const backend = new Programmer("Alex", "27", 3500, "eng");
// const backend = new Programmer();
// backend.name = "Alex";
// backend.age = "27";
backend.salary = 1700;
// backend.lang = "eng";
console.log('backend', backend);
// console.log('name', backend._name);
// console.log('age', backend._age);
// console.log('salary', backend._salary);
// console.log('lang', backend.lang);