// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// Возможность получения данных с сервера. 

// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Література:
// Використання Fetch на MDN
// Fetch
// CSS анімація
// Події DOM

const API = 'https://ajax.test-danit.com/api/swapi'
const card = document.querySelector('.card-wrapper')
const film = document.createElement('div')
// film.setAttribute('id','root')
// card.prepend(film)

const getFilms = () => {
    fetch(`${API}/films`)
    .then (response=> response.json())
    .then (data => {
        console.log(data);
        data.forEach(({name, characters, episodeId, openingCrawl}) => {         
            card.insertAdjacentHTML('beforeend', `
                <div class="film">
                    <h1 class="name">${name}</h1>
                    <h3 class="episode">${episodeId}</h3>
                    <p class="title">${openingCrawl}</p>
                </div>
            `)

            const actors = document.createElement('div')
            actors.setAttribute('class', 'actors')
            characters.forEach((element)=>{
                console.log(element);
                fetch(element)
                    .then(response => response.json())
                    .then(data => {
                        actors.insertAdjacentHTML('beforeend',`<p>${data.name}</p>`)
                    })
            })
            card.append(actors)
        })
    })
}
getFilms ()
